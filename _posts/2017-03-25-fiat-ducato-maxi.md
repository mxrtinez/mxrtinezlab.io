---
title: "Fiat Ducato Maxi (LWB)"
description: "3D model of a van owned by my father created to improve my skills during my spare time. Modelled in 3DS Max, textured in Photoshop."
layout: post

imgpath: "FiatDucatoMaxiLWB/"
ft: "ft.jpg"
sketchfab: "1c2da1dc6b7e4c2295da01cb39ffc0d0"
renders: "1.jpg,2.jpg,3.jpg,4.jpg"
---
*3D model created in **3DS Max** by following a blueprint and has been textured in **Photoshop**.*

By using the edge extrusion method, I've extruded the edges of a plane and I bridged the edges on the top to obtained a very rough shape of the van. I then used various swift loops to soften all the curves, and I used the quick slice tool, among with the cut tool, to create all the shapes (e.g. windows). This time I also experimented with the boolean compound objects by making the inside of the door handles.
 To texture the model, I first separated each object I wanted to texture by detaching them. I then unwrapped and cleaned the UV templates ready to texture them in Photoshop.
Differently from my previous projects, I also experimented with normal maps. This allowed me to add details to the rear lights, head lights and number plates without adding further polygons, but instead using lights and shadows to simulate the additional details. To achieve this, I've used an online normal map generator.
