---
title: "Road Safety (animation)"
description: "3D animation aimed to kids visually explaining how to cross the road. Modelled in 3DS Max, textured in Photoshop, post-processed in Premiere Pro."
layout: post

imgpath: "RoadSafetyAnimation/"
ft: "ft.jpg"
---
*Animation created in **3DS Max** and later edited in **Premiere Pro**.*

As part of my computing course, I've been provided with the task of creating a short video on how to safely cross a road. 

<iframe width="100%" height="315" src="https://www.youtube.com/embed/TP8K0n9-47U" frameborder="0" allowfullscreen></iframe>

I've started by creating a simple road with a footpath, and I then imported my previously modelled vehicles. I also downloaded a character from Adobe Mixamo to use as the main character of my video. To animate all the assets on my scene, I used key frames in 3DS Max with auto key enabled, thus making me set an initial and final point for each asset I wanted to animate without worrying about the frames in between. I later decided to create a second scene showing the character looking in both directions before crossing the road.

Finally, I rendered the two scenes. To do this, I had to render the sequence of frames composing the two scenes. This is because ① if 3DS Max was to crash, I could've simply started rendering from the last frame and not from frame 0 ② because I actually had the first scene split into four parts: 1st part with the red light, 2nd part with the green light, 3rd part with no light at all and 4th part with the red light again. 

In the end, I imported the frames into Premiere Pro which converted all the frames into a single video for me, and I also added some text.
