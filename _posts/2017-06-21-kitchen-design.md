---
title: "Kitchen Design"
description: "A 3D model of a kitchen, modelled in 3DS Max as part of assignment work given a client brief from BTEC."
layout: post

imgpath: "KitchenDesign/"
ft: "ft.jpg"
sketchfab: "eda962f3856a438a9a3917ced956fd86"
renders: "1.jpg,2.jpg,3.jpg"
---
*3D Model made using **3DS Max** and textured in **Photoshop**.*

As part of a BTEC assignment, I was given a client brief to produce a 3D model of a kitchen to be designed from scratch and to be fit for a room with specific dimensions. Before proceeding into 3DS Max, I had to research the average dimensions of every kitchen funrniture and appliance. I then proceeded in modelling a kitchen base unit using the box modelling method. I used the usual swift loop, extrusion and inset tools to model the furniture (such as drawers) and appliances (such as dishwasher and oven). For the extractor fan, I used a bend modifier to obtain the arch effect. I also used a cylinder with a bend modifier, duplicated many times, to make the rack inside the dishwasher. I later added the Unwrap UVW modifier to the dishwasher, oven and extractor fan to render the UVs for each of these, and then used Photoshop to add the controls to these appliances. Finally, I decided to use Mental Ray to render the scene. This was to make the renders look more realistic by using mental ray materials.