---
title: "Land Rover Defender 110"
description: "3D model of a Land Rover Defender 110, modelled in 3DS Max and textured in Photoshop. This model has been featured in a WaterAid VR project from students at LDE UTC."
layout: post

imgpath: "LandRoverDefender110/"
ft: "ft.jpg"
sketchfab: "94bdcfb99fdc4ab584791d1aaf262801"
renders: "1.jpg,2.jpg,3.jpg,4.jpg"
---
*3D Model made using **3DS Max** and textured with **Photoshop**.​*

As I did with the Seat, I started off by placing a plane and then, using the blueprint as a guideline, I extruded the sides of the plane. I manipulated the vertices to create all the shapes (handles, windows), and swift loops to get sharper curves.
In order to texture my model, I had to generate a UV template. I’ve done this by using the UV Editor in 3DS Max. I made the sides, the front and the back of the car larger so I could have added more details to them. On the other hand, the top and the bottom parts of the car were not as important to texture.​
To texture, I’ve used Adobe Photoshop. I used resources from the internet and a very limited set of colours (gray, red and brown) to recreate what I thought were the conditions of a car in Ethiopia. In the end, I exported the texture as a JPEG, and then I applied it as a material to the body of the car in 3DS Max.