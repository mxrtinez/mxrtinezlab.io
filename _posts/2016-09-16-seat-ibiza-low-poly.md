---
title: "Seat Ibiza (low poly)"
description: "Low poly 3D model of a Seat Ibiza 5 doors, modelled in 3DS Max by using a blueprint as a guideline. This has been my first 3D model."
layout: post

imgpath: "SeatIbizaLowPoly/"
ft: "ft.jpg"
sketchfab: "9b454db8b20b43549a7457c964b360dc"
renders: "1.jpg,2.jpg"
---
*3D Model made using **3DS Max** by following a blueprint.*

*First attempt at 3D modelling using 3DS Max.*

I started off by placing a plane on the side of the car, and by using edge extrusion I created the body of the car. I used the cut tool in order to shape the windows, and the extrude tool for the headlights and the bumper. Then, by manipulating vertices and edges, I shaped the car. I also used the material editor to apply the colours. In the end I used the turbosmooth modifier to smooth the edges. I also used keyframe animation with some lighting to create an animation.
