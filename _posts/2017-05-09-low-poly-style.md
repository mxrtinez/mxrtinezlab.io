---
title: "Low Poly Style"
description: "A low poly mountain landscape and a low poly beach I created in 3DS Max to explore the stylised modelling style."
layout: post

imgpath: "LowPolyStyle/"
ft: "ft.jpg"
renders: "1.jpg,2.jpg"
---
*Modelled and rendered in **3DS Max**.*

I wanted to explore the low poly style and therefore spent two afternoons creating two stylised scenes in 3DS Max. The first one is a mountain landscape. This little project introduced me to 'paint deformation', using the push/pull brush to create the mountain. Additionally, I learned how to use the 'hair and fur' modifier, which I used for the grass.
In the second scene, a low poly beach, I familiarised with the noise modifier, which allowed me to easily create a nice, stylised wavy effect on a  plane. I also came across the bend modifier, applying it to the palm leaves. Finally, I discovered GeoSpheres, an alternative to spheres. I used them to make the stones.