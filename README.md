# My website source
Built on Jekyll, based on https://abdu.io HTML concept for school portfolio. 


# Credits
Abdullah Ibne Atiq ([@abdullahibneat](https://twitter.com/abdullahibneat)) for theme concept.

Dave Gamache ([@dhg](https://twitter.com/dhg)) for [Skeleton](http://getskeleton.com/).
